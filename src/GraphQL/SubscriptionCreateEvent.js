import gql from "graphql-tag";

export default gql(`
      subscription{
        subscribeToCreateEvent{
          id
          name
          where
          when
          description
        
        }
      }`);
