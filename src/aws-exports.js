import Amplify from 'aws-amplify';

Amplify.configure({
    Auth: { // STAGING
    // REQUIRED - Amazon Cognito Identity Pool ID
        // identityPoolId: 'us-east-1:ddb6f655-f4ab-4506-a1df-b43405098ef9', // OLD
        identityPoolId: 'us-east-1:f5ca9710-079c-4284-a272-e30d656c4bfc',
    // REQUIRED - Amazon Cognito Region
        region: 'us-east-1',
    // OPTIONAL - Amazon Cognito User Pool ID
        // userPoolId: 'us-east-1:ddb6f655-f4ab-4506-a1df-b43405098ef9',
    // OPTIONAL - Amazon Cognito Web Client ID
        // userPoolWebClientId: 'XX-XXXX-X_abcd1234',
    }
    // Auth: { //PROD
    // // REQUIRED - Amazon Cognito Identity Pool ID
    //     identityPoolId: 'us-east-1:9c3e8676-4f4e-4d13-9cf9-6d88fd93cea9',
    // // REQUIRED - Amazon Cognito Region
    //     region: 'us-east-1',
    // // OPTIONAL - Amazon Cognito User Pool ID
    //     // userPoolId: 'us-east-1:ddb6f655-f4ab-4506-a1df-b43405098ef9',
    // // OPTIONAL - Amazon Cognito Web Client ID
    //     // userPoolWebClientId: 'XX-XXXX-X_abcd1234',
    // }
});

export default Amplify;
