import gql from "graphql-tag";

export default gql(`
mutation MutationDeleteEvent($eventId: ID!, $commentId : [ID]!) {
  deleteEvent(commentId: $commentId,eventId: $eventId){
    id
  }
}`);


// deleteEvent(
// commentId: ["21319ca6-4c88-4269-b2e8-55a6dd0217f9"],
// eventId:"b014544b-9974-4eeb-b25c-5d0b73e6628f"){
//   id
// }
//
// name
// where
// when
// description
// comments {
//   items {
//     commentId
//   }
// }
