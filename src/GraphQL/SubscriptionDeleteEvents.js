import gql from "graphql-tag";

export default gql(`
      subscription {
        subscribeToDeleteEvent {
          id
        }
      }`);
