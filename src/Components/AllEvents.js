import React, { Component } from "react";
import { Link } from "react-router-dom";
import {NotificationContainer, NotificationManager} from 'react-notifications';

import { graphql, compose, withApollo } from "react-apollo";
import QueryAllEvents from "../GraphQL/QueryAllEvents";
import MutationDeleteEvent from "../GraphQL/MutationDeleteEvent";
import SubsriptionDeleteEvents from "../GraphQL/SubscriptionDeleteEvents";
import SubsriptionCreateEvents from "../GraphQL/SubscriptionCreateEvent";

import moment from "moment";
sessionStorage.setItem('handledSyncCalled',false);
var x = false;
class AllEvents extends Component {
    state = {
        busy: false,
        handledSyncCalled : false
    }
    subscription;

    componentDidMount() {
        // console.log('componentDidMount  :: props ',this.props);
        this.subscribeDeleteEvent = this.props.subscribeToDeleteEvents();
        this.subscribeCreateEvent = this.props.subscribeToCreateEvents();
    }

    componentWillUnmount() {
  // console.log('componentWillUnmount  :: props ',this.props);
        this.subscribeDeleteEvent();
        this.subscribeCreateEvent();
    }
  componentWillMount(){
console.log('componentWillMount  :: sessionStorage.getItem ',typeof sessionStorage.getItem('handledSyncCalled'),sessionStorage.getItem('handledSyncCalled'));
    if(sessionStorage.getItem('handledSyncCalled') == 'false'){

      console.log('handleSync called ');
      this.handleSync();
      sessionStorage.setItem('handledSyncCalled',true);
    }
  }
    static defaultProps = {
        events: [],
        deleteEvent: () => null,
        subscribeToDeleteEvents: () => null,
        subscribeToCreateEvents: () => null
    }

    async handleDeleteClick(event, e) {
      // console.log('handleDeleteClick :: ',event);
      var commentIdArr = [];

        if(event.comments && event.comments.items.length > 0){
          for(var i in event.comments.items){
            commentIdArr.push(event.comments.items[i].commentId);
          }
        }
        // console.log('commentIdArr :: ',commentIdArr);
        e.preventDefault();

        if (window.confirm(`Are you sure you want to delete event ${event.id}`)) {
            const { deleteEvent } = this.props;

            await deleteEvent(event,commentIdArr);
        }
    }

    handleSync = async () => {
      console.log('handleSync called');
        const { client } = this.props;
        const query = QueryAllEvents;
        // console.log('handleSync this.props :: ',this.props);
        this.setState({ busy: true });

        await client.query({
            query,
            fetchPolicy: 'network-only',
        });

        this.setState({ busy: false });
    }


    createNotification = (type) => {
    return () => {
      switch (type) {
        case 'info':
          NotificationManager.info('Info message');
          break;
        case 'success':
          NotificationManager.success('Success message', 'Title here');
          break;
        case 'warning':
          NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);
          break;
        case 'error':
          NotificationManager.error('Error message', 'Click me!', 5000, () => {
            alert('callback');
          });
          break;
      }
    }
  }

    renderEvent = (event) => (
        <Link to={`/event/${event.id}`} className="card" key={event.id}>
            <div className="content">
                <div className="header">{event.name}</div>
            </div>
            <div className="content">
                <p><i className="icon calendar"></i>{moment(event.when).format('LL')}</p>
                <p><i className="icon clock"></i>{moment(event.when).format('LT')}</p>
                <p><i className="icon marker"></i>{event.where}</p>
            </div>
            <div className="content">
                <div className="description"><i className="icon info circle"></i>{event.description}</div>
            </div>
            <div className="extra content">
                <i className="icon comment"></i> {(event.comments) ? event.comments.items.length : 0} comments
            </div>
            <button className="ui bottom attached button" onClick={this.handleDeleteClick.bind(this, event)}>
                <i className="trash icon"></i>
                Delete
            </button>
        </Link>
    );

    render() {
        const { busy } = this.state;
        const { events } = this.props;

        // console.log('this.props :: ',this.props);
        return (
            <div>
                <div className="ui clearing basic segment">
                    <h1 className="ui header left floated">All Events</h1>
                    <button className="ui icon left basic button" onClick={this.handleSync} disabled={busy}>
                        <i aria-hidden="true" className={`refresh icon ${busy && "loading"}`}></i>
                        Sync with Server
                    </button>
                </div>
                <div className="ui link cards">
                    <div className="card blue">
                        <Link to="/newEvent" className="new-event content center aligned">
                            <i className="icon add massive"></i>
                            <p>Create new event</p>
                        </Link>
                    </div>
                    {[].concat(events).sort((a, b) => b.when.localeCompare(a.when)).map(this.renderEvent)}
                </div>
            </div>
        );
    }

}

export default withApollo(compose(
    graphql(
        QueryAllEvents,
        {
            options: {
                fetchPolicy: 'cache-first',
            },
            props: props => ({
                events: props.data.listEvents && props.data.listEvents.items ? props.data.listEvents.items : { items: [] },
                subscribeToDeleteEvents: () =>{
                  // console.log('props.data :: ',props.data);
                  return props.data.subscribeToMore({
                      document: SubsriptionDeleteEvents,
                      updateQuery: (prev, { subscriptionData: { data: { subscribeToDeleteEvent } } }) => {
                        // console.log('delete subscribe :: ',this,props);
                        alert('Event ID :: '+subscribeToDeleteEvent.id+' has been deleted');
                          const res = {
                              ...prev,
                              listEvents: {
                                __typename: 'EventConnection',
                                items : [ ...prev.listEvents.items.filter(c => c.id !== subscribeToDeleteEvent.id)]
                          }
                        }

                          return res;
                      }
                  });
                },
                subscribeToCreateEvents: () =>{

                  // console.log('props.data :: ',props.data);
                  return props.data.subscribeToMore({
                      document: SubsriptionCreateEvents,
                      // variables: {
                      //     eventId: props.ownProps.eventId,
                      // },
                      updateQuery: (prev, { subscriptionData: { data: { subscribeToCreateEvent } } }) => {
                        // console.log('Event ID :: ',subscribeToCreateEvent);
                        alert('Event '+subscribeToCreateEvent.name+ ' has been added');
                          const res = {
                              ...prev,
                              listEvents: {
                                __typename: 'EventConnection',
                                items : [ ...prev.listEvents.items.filter(c => c.id !== subscribeToCreateEvent.id),subscribeToCreateEvent]
                          }
                        }

                          return res;
                      }
                  });
                },
            }),
        }
    ),
    graphql(
        MutationDeleteEvent,
        {
            options: {
                update: (proxy, { data: { deleteEvent } }) => {
                    const query = QueryAllEvents;
                    const data = proxy.readQuery({ query });

                    data.listEvents.items = data.listEvents.items.filter(event => event.id !== deleteEvent.id);

                    proxy.writeQuery({ query, data });
                }
            },
            props: props => ({
                deleteEvent: function(event,commentIdArr){
                  return props.mutate({
                      variables: { eventId: event.id, commentId : commentIdArr },
                      optimisticResponse: () => ({
                          deleteEvent: {
                              ...event, __typename: 'Event', comments: { __typename: 'CommentConnection', items: [] }
                          }
                      }),
                  });
                },
      }
    )
  })
)(AllEvents));





// props: ({ data: { listEvents = { items: [] } } }) => ({
//     events: listEvents.items
// })

// subscribeToDeleteEvents: () =>{
//   console.log('props.data :: ',props.data,this.props,props);
//   // return props.data.subscribeToMore({
//   //     document: SubsriptionDeleteEvents,
//   //     variables: {
//   //         eventId: props.ownProps.eventId,
//   //     },
//   //     updateQuery: (prev, { subscriptionData: { data: { subscribeToDeleteEvent } } }) => {
//   //         const res = {
//   //             ...prev,
//   //             listEvents: {
//   //                 ...prev.listEvents,
//   //                 event: {
//   //                     __typename: 'Event',
//   //                     ...prev.listEvents,
//   //                     items: [
//   //                         ...prev.listEvents.items.filter(c => c.id !== subscribeToDeleteEvent.id),
//   //                         subscribeToDeleteEvent,
//   //                     ]
//   //                 }
//   //             }
//   //         };
//   //
//   //         return res;
//   //     }
//   // });
// }









/*  props: (props) => ({
      // deleteEvent: (event,commentIdArr) => {
      //     return props.mutate({
      //         variables: { id: event.id, commentId : commentIdArr },
      //         optimisticResponse: () => ({
      //             deleteEvent: {
      //                 ...event, __typename: 'Event', comments: { __typename: 'CommentConnection', items: [] }
      //             }
      //         }),
      //     });
      // },
      subscribeToDeleteEvent: () =>{
        console.log('props :: ',props);
      //  return props.data.subscribeToMore({
      //     document: SubsriptionDeleteEvents,
      //     variables: {
      //         eventId: props.ownProps.eventId,
      //     },
      //     updateQuery: (prev, { subscriptionData: { data: { subscribeToDeleteEvent } } }) => {
      //         const res = {
      //             ...prev,
      //             listEvents: {
      //                 ...prev.listEvents,
      //                 event: {
      //                     __typename: 'Event',
      //                     ...prev.listEvents,
      //                     items: [
      //                         ...prev.listEvents.items.filter(c => c.id !== subscribeToDeleteEvent.id),
      //                         subscribeToDeleteEvent,
      //                     ]
      //                 }
      //             }
      //         };
      //
      //         return res;
      //     }
      // })

    }
  }) */
